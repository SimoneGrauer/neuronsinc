<?php 

get_header();
?>
<main id="main" class="site-main">
<!-- HERO SECTION -->
<?php include('code_blocks/hero.php');?>

<?php if( have_rows('content_blocks') ): ?>
    <?php while( have_rows('content_blocks') ): the_row(); ?>
        <?php if( get_row_layout() == 'banner' ): ?>
            <?php $smallHeadline = get_sub_field('banner_small_headline');?>
            <?php $headline = get_sub_field('banner_headline'); ?>
            <?php $text = get_sub_field('banner_text');?>
            <?php $download = get_sub_field( 'download_file' ); ?>
            <?php $downloadBtnText = get_sub_field('banner_download_text')?>
            <?php $bannerButton = get_sub_field( 'banner_link' ); ?>
            <?php $bannerBtnText = get_sub_field('banner_button_text') ?>
            <?php $bannerImage = get_sub_field('banner_image'); ?>
            <?php include('code_blocks/banner.php');?>
            
            <?php elseif( get_row_layout() == 'how_to_block' ):?>
                <?php if( have_rows('steps') ):?>
                    <div class="lightPurple site-padding-both">
                        <div class="container how-to-container">
                    <?php while( have_rows('steps') ) : the_row();?>
                            <div class="how-to-content">
                                <h1><?php the_sub_field('headline'); ?></h1>
                                <?php $icon = get_sub_field('icon'); ?>
                                <?php if( $icon): ?>
                                <div class="how-to-icon" style="background-image: url('<?php echo esc_url( $icon['sizes']['medium'] ); ?>');">
                                </div>
                                <?php endif;?>
                                <p><?php the_sub_field('text'); ?></p>
                            </div>    
                    <?php endwhile;?>
                         </div>
                    </div>
                <?php endif;?>
            <?php elseif( get_row_layout() == 'try_solution' ):?>
                <div class="try-solution container site-padding-both">
                    <h2><?php the_sub_field('small_headline'); ?></h2>
                    <h1><?php the_sub_field('headline'); ?></h1>
                    <!-- <div class="try-embeded">
                        <form id="file" action="#" onsubmit="imgToBase64();return false">
                            <input type="file" name="image">
                            <input type="submit">
                        </form>
                    </div> -->
                </div>
            <?php elseif( get_row_layout() == 'cases' ):?>
                <div class="cases-contianer container site-padding-bottom">
                    <h2><?php the_sub_field('small_headline'); ?></h2>
                    <h1><?php the_sub_field('headline'); ?></h1>
                    <?php $cases = get_sub_field('cases_display');
                        if( $cases ): ?>
                        <div class="posts-container">
                            <?php foreach( $cases as $case ): 
                                $link = get_permalink( $case->ID );
                                $title = get_the_title( $case->ID );
                                $desc = get_field('small_text', $case->ID);
                                $postImage = get_field('image', $case->ID);
                                $clientLogo = get_field('client_logo', $case->ID);
                                $maxLength = 250;
                                if (strlen($desc) <= $maxLength){
                                  $desc = $desc;
                                }else{
                                  $desc =  substr($desc, 0,  $maxLength).'...';
                                }
                                ?>          
                                <div class='post-container site-padding-bottom'>
                                    <div class='post-image' style='background-image: url("<?= $postImage['url'];?>")'></div>
                                        <div class='post'>
                                            <h1><?= $title;?></h1>
                                            <p><?= $desc;?></p>
                                            <a href='<?= $link;?>'>
                                                <button class='button secondary-btn'>Read <strong>more</strong></button>
                                            </a>
                                         </div>
                                  </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                    </div>
            <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>
    
</main>
<?php
get_footer();