<?php 
/* Template Name: Insights */
get_header();
?>
<main id="main" class="site-main">
<?php include('code_blocks/sub-hero.php');?>


<div class="category-tabs container">

    <a class="cat-filter-item" href="<?=home_url()?>">All</a>
<?php
$catArgs = array(
    'exclude' => array(1),
    'option_all' => 'All'
);

$categories = get_categories($catArgs);

foreach($categories as $cat):?>
  <a class="cat-filter-item" data-category="<?= $cat->term_id;?>" href="<?= get_category_link($cat->term_id);?>"><?= $cat->name?></a>
<?php endforeach;?>

</div>


<div class="insights-container posts-container container site-padding-both">
<?php $args = array(
    'post_type' => 'insights',
    'post_per_page' => -1
);

$query = new WP_Query($args);

if($query->have_posts()):
    while($query->have_posts()) : $query->the_post();?>
        <div class='post-container'>
			<?php $image = get_field('image');?>
            <div class='post-image' style='background-image: url(<?php echo esc_url( $image['sizes']['medium_large'] ); ?>)'></div>
            <div class='post'>
                <?php $date = date_create(get_field('publish_date')); 
                  $date = date_format($date, "M j, Y");?>
              <p><?php echo $date;?> - <?php the_field('author');?></p>
              <h1><?php the_field('headline');?></h1>
              <p><?php the_field('small_text');?></p>
              <?php $readLink = get_permalink()?>
              <div class="post-link">
              <a href='<?php echo esc_url( $readLink );?>'>
              <button class='button secondary-btn'>Read <strong>more</strong></button>
              </a>
              </div>
            </div>
        </div>
<?php
    endwhile;
endif;
wp_reset_postdata();
?>
</div>

</main>
<?php
get_footer();