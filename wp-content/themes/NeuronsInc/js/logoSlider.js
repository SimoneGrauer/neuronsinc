let index = 0;
let order = 1;
const speed = 3;
const numberOfSlides = document.querySelectorAll('.carouselItem').length;
const carouselContainer = document.querySelector(".carouselContainer");
const carouselItemWidth = carouselContainer.scrollWidth / numberOfSlides;
setInterval(() => {
	carouselContainer.scrollBy({left: carouselItemWidth, top: 0, behavior: 'smooth'});
	timeoutId = setTimeout(() => {
		index = index % numberOfSlides;
		let childToMove = carouselContainer.querySelectorAll('.carouselItem')[index];
		childToMove.style.order = order;
		index++;
		order++;
		clearTimeout(timeoutId);
	}, 1000);
}, speed * 1000);
