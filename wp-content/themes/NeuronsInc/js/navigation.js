var prevScrollpos = window.pageYOffset;
var pixelScrolled = 0;
var delaydHide = 25;
const positionForWhiteBg = 50;
const navigation = document.querySelector('.nav-container');
const siteLogo = document.querySelector('.logo');
const navigationContent = document.getElementById('navContentDesktop');
if(window.innerWidth < 768 ){delaydHide = 75;}

window.addEventListener('scroll', scrolling);
window.addEventListener('DOMContentLoaded', navStateOnLoad);

function scrolling() {
	var currentScrollPos = window.pageYOffset;
		pixelScrolled++;
		if (prevScrollpos > currentScrollPos) {
			toggleNav('show');
			if(currentScrollPos > positionForWhiteBg){
				toggleNavBackground('thin');
			} else {
				toggleNavBackground('normal');
			}
		} else {
			if (pixelScrolled > delaydHide){
				toggleNav('hide');
			}
		}
		prevScrollpos = currentScrollPos;
	}
	
function toggleNav(state){
	if(state == 'hide'){
		if (!navigation.classList.contains('hideNav')) {
			navigation.classList.add('hideNav');
		}
	} else if (state == 'show'){
		if (navigation.classList.contains('hideNav')) {
			navigation.classList.remove('hideNav');
			pixelScrolled = 0;
		}
	}
}
function toggleNavBackground(state){
	if(state == 'thin'){
		navigation.classList.add('bg-color-white');
		navigationContent.classList.add('thinNav');
		siteLogo.classList.add('thinNav');
	} else if (state == 'normal'){
		navigation.classList.remove('bg-color-white');
		navigationContent.classList.remove('thinNav');
		siteLogo.classList.remove('thinNav');
		

	}
}
function navStateOnLoad(state){
	var currentScrollPos = window.pageYOffset;
	if(currentScrollPos > positionForWhiteBg){
		toggleNavBackground('thin');
	} else {
		toggleNavBackground('normal');
	}
}

/// Sub menu

const navItems = document.querySelectorAll('.navdesktop');
const subMenuItems = document.querySelectorAll('meta[name="submenuitem"]');
const subMenuContainerId = 'submenu';
const subMenuContainer = document.getElementById(subMenuContainerId);
const subMenuOutput = document.getElementById('submenuitems');
navItems.forEach((item, i) => {
	item.addEventListener('mouseover', findSubMenuItems);
});
subMenuContainer.addEventListener('mouseout', hideSubMenu);
function findSubMenuItems(){
	let hoveredId = event.target.dataset.myid;
	let hoveredEl = event.target;
	let matchedSubMenuItems = [];
	subMenuItems.forEach((item, i) => {
		if (hoveredId == item.dataset.parent) {
			var obj = {};
			obj.myid = item.dataset.myid;
			obj.title = item.dataset.title;
			obj.url = item.dataset.link;
			obj.parent = item.dataset.parent;
			obj.isParent = true;
			matchedSubMenuItems.push(obj);
			subMenuItems.forEach((subitem, i)=>{
				if(item.dataset.myid == subitem.dataset.parent){
					var subObj = {};
					subObj.childOf = subitem.dataset.parent;
					subObj.title = subitem.dataset.title;
					subObj.url = subitem.dataset.link;
					matchedSubMenuItems.push(subObj);
				}
			})
		}


	});
	(matchedSubMenuItems.length > 0) ? outputSubMenuItems(matchedSubMenuItems) : hideSubMenu('force');

	
	var hoveredPosition = hoveredEl.getBoundingClientRect().x;
	subMenuOutput.style.marginLeft = hoveredPosition+'px';
}

function outputSubMenuItems(objOfMatches){
	displaySubMenu();
	clearCurrentItems();
	objOfMatches.forEach((item, i) => {
		createMenuItem(item);
	});
}

function createMenuItem(item){
	if(item.isParent === true){
		var container = document.createElement('div');
		container.classList.add('desktop-subitem');
		container.id = item.myid;
		var parentMenuItem = document.createElement('a');
		parentMenuItem.classList.add('parent-sub-item');
		parentMenuItem.innerText = item.title;
		parentMenuItem.href = item.url;
		container.appendChild(parentMenuItem);
		subMenuOutput.appendChild(container);
	} else {
		var menuItem = document.createElement('a');
		menuItem.href = item.url;
		menuItem.innerText = item.title;
		menuItem.dataset.parent = item.childOf;
		menuItem.classList.add('subMenuItem')
		var outputTo = document.getElementById(item.childOf);
		outputTo.appendChild(menuItem);
	}
}

function clearCurrentItems(){
	subMenuOutput.innerHTML = '';
}
function displaySubMenu(){
	subMenuContainer.classList.remove('hidden');
	// lav baggrundsfarve på navigation
	document.querySelector(".nav-container").classList.add("white-bg-nav");
}
function hideSubMenu(force){
	if(event.relatedTarget.closest('main') || event.relatedTarget.closest('footer') ||force == 'force'){
		subMenuContainer.classList.add('hidden');
		document.querySelector(".nav-container").classList.remove("white-bg-nav");
		clearCurrentItems();
	}
	window.addEventListener('scroll', () => {
		subMenuContainer.classList.add('hidden');
		document.querySelector(".nav-container").classList.remove("white-bg-nav");
		clearCurrentItems();
	})
}

const logo = document.querySelector('.logo');
logo.addEventListener('click', function(){
    window.location.href='https://simone-grauer.dk/neurons';
})