const solutionTabs = document.querySelectorAll('.solution-tabs-container p');
const categoryTabs = document.querySelectorAll('.area-tabs-container button');
let activeSolution = document.querySelectorAll('.content');
let activeCategory  = document.querySelectorAll('.category-image-container');

solutionTabs.forEach((item, i) => {
  item.addEventListener('click', displaySolution)
});

function displaySolution(query){
  // control active classes
  document.querySelectorAll('.solution-tabs-container .active').forEach((item)=>{item.classList.remove('active');})
  var displayThisType;
  if (event.type !== 'click') {
    console.log(query);
    displayThisType = query;
    document.querySelector('[data-show="'+query+'"]').classList.add('active');
    query = false;
  } else {
    displayThisType = event.target.dataset.show;
    event.target.classList.add('active');
  }
  activeSolution.forEach((item, i) => {
    if(displayThisType == item.dataset.show){
        item.classList.add('show');
    } else {
        item.classList.remove('show');
    }
  });

}
  function displayCategory(query){
    // control active classes
    document.querySelectorAll('.area-tabs-container .active').forEach((tab)=>{tab.classList.remove('active');})
    var displayType;
    if (event.type !== 'click') {
      console.log(query);
      displayType = query;
      document.querySelector('[data-show="'+query+'"]').classList.add('active');
      query = false;
    } else {
      displayType = event.target.dataset.show;
      event.target.classList.add('active');
    }
    activeCategory.forEach((tab, i) => {
      if(displayType == tab.dataset.show){
          tab.classList.add('show');
      } else {
          tab.classList.remove('show');
      }
    });
  }








