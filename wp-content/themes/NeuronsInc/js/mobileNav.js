window.addEventListener("DOMContentLoaded", init);

const mobileNavItems = document.querySelectorAll('.mobileItem');
const mobileSubItems = document.querySelectorAll('meta[name="mobilesubitem"]');
let mobilnavitem = document.querySelector('.mobileItem');
let buttonId = mobilnavitem.dataset.myid;
let subitemsContainers = document.querySelectorAll('.subitems-container')

function init(){
    mobileNavItems.forEach((mobileItem, i)=>{
        buttonId = mobileItem.dataset.myid;
        findsubItems();
    });
}

mobileNavItems.forEach((mobilenavitem, i)=>{
    mobilenavitem.addEventListener("click", function() {
    let clickedId = event.target.dataset.myid;
    subitemsContainers.forEach((subContainer, i)=>{
        if(clickedId === subContainer.dataset.parent){
            subContainer.classList.toggle('show-mobile');
            mobilenavitem.classList.toggle('hideLine');
            mobilenavitem.classList.toggle('turn');
        }
        })
        
    })
})

function findsubItems(){
    let matchedSubItems = [];
    mobileSubItems.forEach((mobileItem, i)=>{
        if(buttonId === mobileItem.dataset.parent){
            let obj = {};
            obj.myid = mobileItem.dataset.myid;
            obj.title = mobileItem.dataset.title;
            obj.url = mobileItem.dataset.link;
            obj.parent = mobileItem.dataset.parent;
            matchedSubItems.push(obj);
        }
    })
    console.log(matchedSubItems)
    outputSubItems(matchedSubItems);
}

function outputSubItems(objMatches){
    objMatches.forEach((mobileItem, i) => {
		createSubItem(mobileItem);
	});
}

function createSubItem(mobileItem){
    let menusubItem = document.createElement('a');
    menusubItem.classList.add('mobile-subitem');
    menusubItem.href = mobileItem.url;
    menusubItem.innerText = mobileItem.title;
    menusubItem.dataset.parent = mobileItem.parent;
    var outputTo = document.getElementById(mobileItem.parent);
    outputTo.appendChild(menusubItem);
    

}

document.getElementById('burgerButton').addEventListener('click', function() {
	this.classList.toggle('animate');
	document.getElementById('navContentMobile').classList.toggle('hidden');
	document.body.classList.toggle('scroll-lock');
})

const mobileLogo = document.querySelector('.mobilelogo');
mobileLogo.addEventListener('click', function(){
    window.location.href='https://simone-grauer.dk/neurons';
})

