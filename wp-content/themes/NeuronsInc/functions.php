<?php 
/** disable wp toolbar **/
add_filter('show_admin_bar', '__return_false');

/* Wordpress controls title-tag */
add_theme_support( 'title-tag' );


/* Create options with acf */
if( function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' => 'Navigation'
		)
	);
	acf_add_options_page(array(
		'page_title' => 'Footer'
		)
	);
}

/* Creates a menu in wordpress under appereances */
register_nav_menus(
	array(
		'Primary' => esc_html__( 'Primary', 'neuronsinc' ),
	)
);

/* Hide default editor on pages */
add_action( 'admin_init', 'hide_editor' );

function hide_editor() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;

    $template_file = get_post_meta($post_id, '_wp_page_template', true);

		$hideOn = [
			'frontpage.php',
			'insights.php',
			'contact.php',
			'about.php',
			'positions.php'
		];


    if(in_array($template_file, $hideOn) ){ 
        remove_post_type_support('page', 'editor');
    }
}
?>