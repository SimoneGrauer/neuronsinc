<?php 
/* Template Name: Contact */
get_header();
?>
<main id="main" class="site-main">
    <div class="contact-container container">
        <div class="contact-text-content">
            <h2><?php the_field('small_headline');?></h2>
            <h1><?php the_field('headline');?></h1>
            <p><?php the_field('text');?></p>
        </div>
        <div class="contact-form">
            
        </div>
    </div>
</main>
<?php
get_footer();