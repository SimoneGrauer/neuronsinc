<?php 
/* Template Name: Frontpage */
get_header();
?>
<main id="main" class="site-main">
    <!-- HERO SECTION -->
    <?php include('code_blocks/hero.php');?>

    <!-- LOGO BANNER SECTION --> 
    <div id="logoBanner" class="lightPurple">
        <div class="container slideout-icons">
            <div class="carouselWrapper">
                <div class="carouselContainer">
                    <?php for ($i=0; $i < 1; $i++):?>
                      <?php if ( have_rows( 'logos' ) ) : ?>
                        <?php while ( have_rows( 'logos' ) ) : the_row(); ?>
                          <?php $logo = get_sub_field( 'logo' ); ?>
                          <?php if ( $logo ) { ?>
                            <div class="carouselItem client-logo">
                                <img loading="lazy" src="<?php echo $logo['sizes']['medium']; ?>" alt="">
                            </div>
                          <?php } ?>
                        <?php endwhile; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- SOLUTIONS SECTION -->
    <div id="solutions" class="container">
        <div id="solutions" class="site-padding-both">
            <h2><?php the_field('solutions_small_headline');?></h2>
            <h1><?php the_field('solutions_headline');?></h1>
            <?php if ( have_rows( 'solutions' ) ) : ?>
            <div class="solution-tabs-container">
                <?php $i = 0; ?>
                <?php while ( have_rows( 'solutions' ) ) : the_row(); ?>
                <p class="solution-tab <?php if($i == 0){echo 'active';} ?>" data-show="<?php echo str_replace(' ','',get_sub_field( 'name_for_tab' )); ?>"><?php the_sub_field( 'name_for_tab' ); ?></p>
                <?php $i++; ?>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'solutions' ) ) : ?>
            <div class="solution-content">
            <?php $i = 0; ?>
                <?php while ( have_rows( 'solutions' ) ) : the_row(); ?>
                    <div class="content hidden <?php if($i == 0){echo 'show';} ?>" data-show="<?php echo str_replace(' ','',get_sub_field( 'name_for_tab' )); ?>">
                        <div class="solutions-grid">
                            <div class="solution-text-content">
                        <h2><?php the_sub_field( 'fp_small_headline' ); ?></h2> 
                        <h1><?php the_sub_field( 'fp_headline' ); ?></h1> 
                    <?php if ( have_rows( 'fp_images' ) ) : ?>
                        <?php $i = 0; ?>
                        <div class="category-tab-grid">
                        <?php while ( have_rows( 'fp_images' ) ) : the_row(); ?>
                        <div class="area-tabs-container">
                        <?php $categoryIcon = get_sub_field('solution_category_icon'); ?>
                            <?php if( !empty( $categoryIcon ) ): ?>
                                <div class="category-icon">
                                <img src="<?php echo esc_url($categoryIcon['url']); ?>" alt="<?php echo esc_attr($categoryIcon['alt']); ?>" />
                                </div>
                            <?php endif; ?>
                            <button class="category-tab <?php if($i == 0){echo 'active';} ?>" data-show="<?php echo str_replace(' ','',get_sub_field( 'solution_category' )); ?>" onclick="displayCategory()"><?php the_sub_field( 'solution_category' ); ?></button>
                        </div>
                        <?php $i++; ?>
                <?php endwhile; ?>
                </div>
            <?php endif;?>
                <p><?php the_sub_field( 'fp_text' ); ?></p>
                <div class="cta-buttons-container">
                <?php $productBtn = get_sub_field( 'link_to_solution' ); ?>
                <a href="<?php echo esc_url( $productBtn['url'] ); ?>" rel="nofollow" target="_blank">
                    <button class="button secondary-btn"><?php the_sub_field( 'link_to_solution_text' ); ?></button>
                </a>
                <?php $hubBtn = get_sub_field( 'link_to_neuronshub' ); ?>
                <a href="<?php echo esc_url( $hubBtn['url'] ); ?>" rel="nofollow" target="_blank">
                    <button class="button primary-btn"><?php the_sub_field( 'link_to_neuronshub_text' ); ?></button>
                </a>
                </div>
                </div>
            <?php if ( have_rows( 'fp_images' ) ) : ?>
                <?php $i = 0; ?>
                <div class="category-image-parent">
                <?php while ( have_rows( 'fp_images' ) ) : the_row(); ?>
                    <div class="category-image-container hidden <?php if($i == 0){echo 'show';} ?>" data-show="<?php echo str_replace(' ','',get_sub_field( 'solution_category' )); ?>">
                    <?php $categoryImage = get_sub_field('fp_image'); ?>
                    <?php if( !empty( $categoryImage ) ): ?>
                        <img src="<?php echo esc_url($categoryImage['url']); ?>" alt="<?php echo esc_attr($categoryImage['alt']); ?>" />
                    <?php endif; ?>
                    <?php $categoryVideo = get_sub_field( 'video' )?>
                    <?php if( $categoryVideo ) : ?>
			        	<video class="single-video" width="100%"  preload autoplay loop muted>
			        		<source src="<?php echo $categoryVideo['url'] ?>" type="video/mp4"/>
			        	</video>
			        
                    <?php endif;?>
                    </div>
                    
                <?php $i++; ?>
            <?php endwhile; ?>
            </div>
            <?php endif;?>
                </div>
                <?php $i++; ?>
                </div>
            <?php endwhile; ?>
                </div>
            <?php endif;?>
        </div>
    </div>

   <!-- BANNER SECTION -->
    <?php $smallHeadline = get_field('banner_small_headline');?>
    <?php $headline = get_field('banner_headline'); ?>
    <?php $text = get_field('banner_text');?>
    <?php $download = get_field( 'download_file' ); ?>
    <?php $downloadBtnText = get_field('banner_download_text')?>
    <?php $bannerButton = get_field( 'banner_link' ); ?>
    <?php $bannerBtnText = get_field('banner_button_text') ?>
    <?php $bannerImage = get_field('banner_image'); ?>
    <?php include('code_blocks/banner.php');?>


<?php if (1==1):?>
  <?php
  $numberOfposts = 3;
  $arguments = array (
  'post_type' => array('insights'),
  'posts_per_page'   => $numberOfposts,
  'paged' => 1,
  'post_status'=>'publish',
   'meta_query' => array(
        array(
            'key' => 'publish_date',
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        )
    ),
    'post_status'=>'publish',
    'meta_key' => 'publish_date',
    'orderby' => 'publish_date',
    'order' => 'DSC',
    'numberposts'=> '3'

     ); ?>

<?php $insightPosts = new WP_Query($arguments);?>
<?php $insightPosts = $insightPosts->posts; ?>

<div class="container site-padding-top insights-container">
    <h2>Stay updated with our</h2>
    <h1>Newest <strong>insights</strong></h1>
    <div class="posts-container site-padding-bottom">
<?php
        $i = 0;
        $insightsMeta = [];
        foreach($insightPosts as $insightPost){
          $postId = $insightPost->ID;
          $insightsMeta[$i] = get_post_meta($postId);
          $dateCreated = date_create($insightsMeta[$i]['publish_date'][0]);
          $dateCreated = date_format($dateCreated, "M j, Y");
          $author = $insightsMeta[$i]['author'][0];
          $title = $insightsMeta[$i]['headline'][0];
          $smallText = $insightsMeta[$i]["small_text"][0];
          $postImage = wp_get_attachment_url($insightsMeta[$i]['image'][0]);
          
          $maxLength = 200;
          if (strlen($smallText) <= $maxLength){
            $smallText = $smallText;
          }else{
            $smallText =  substr($smallText, 0,  $maxLength).'...';
          }
          $link = get_permalink($postId);
        
        echo "<div class='post-container site-margin-bottom'>
                <div class='post-image' style='background-image: url($postImage)'></div>
                  <div class='post'>
                    <p>$dateCreated - $author</p>
                    <h1>$title</h1>
                    <p>$smallText</p>
                    <div class='post-link'>
                    <a href='$link'>
                    <button class='button secondary-btn'>Read <strong>more</strong></button>
                    </a>
                    </div>
                  </div>
              </div>";
              
            }
      ?>
      </div>
      </div>
<?php endif; ?>
</main>
<?php
get_footer();