<?php 
/* Template Name: Open positions */
get_header();
?>
<?php include('code_blocks/sub-hero.php');?>
<?php if (1==1):?>
  <?php
  $arguments = array (
  'post_type' => 'positions',
  'posts_per_page'   => $numberOfposts,
  'paged' => 1,
  'post_status'=>'publish',
  /*  'meta_query' => array(
        array(
            'key' => 'publish_date',
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        )
    ), */
    'post_status'=>'publish',
    'order' => 'DSC',
     ); ?>

<?php $positions = new WP_Query($arguments);?>
<?php $positions = $positions->posts; ?>

<div class="container site-padding-top position-container">

    
<?php
        $i = 0;
        $positionsMeta = [];
        foreach($positions as $position){
          $postId = $position->ID;
          $positionsMeta[$i] = get_post_meta($postId);
          $title = $positionsMeta[$i]['headline'][0];
          $smallText = $positionsMeta[$i]["text"][0];
          $postImage = wp_get_attachment_url($positionsMeta[$i]['image'][0]);
          $Link = get_field('apply_link', $postId);
          $applyLink = $Link['url'];
          $linkPDF = get_field('read_pdf_file', $postId);
          $pdfLink = $linkPDF['url'];
          
        
        echo "<div class='position-child'>
                  <div class='post'>
                    <div class='post-image-container'>
                        <div class='post-image' style='background-image: url($postImage)'></div>
                    </div>
                    <div class='post-content-container'>
                  <h2>Open position</h2>
                    <h1>$title</h1>
                    <p>$smallText</p>
                    <div class='cta-buttons-container position-buttons'>
                    <a href='$applyLink'>
                    <button class='button primary-btn'>Apply <strong>now</strong></button>
                    </a>
                    <a href='$pdfLink'>
                    <button class='button secondary-btn'>Read <strong>PDF</strong></button>
                    </a>
                    </div>
                    </div>
                  </div>
              </div>";
              
            }
      ?>

      </div>
<?php endif; ?>
<?php
get_footer();
