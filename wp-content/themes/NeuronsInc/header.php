<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- <meta charset="<?php bloginfo( 'charset' ); ?>"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri();?>/style.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet"> 
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri();?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri();?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri();?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri();?>/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri();?>/favicon/safari-pinned-tab.svg" color="#8166d1">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="theme-color" content="#ffffff">
</head>	

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
    <?php
	$menu_name = 'Primary';
	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	$menuitems = wp_get_nav_menu_items( $menu_name, array( 'order' => 'DESC' ) );
	?>
	<nav class="nav-container">
		<div id="navContentDesktop" class=" nav-background">
			<div class="nav-content container">
				<div class="logo">
					<?php $neuronsLogo = get_field('logo', 'option');
					if( !empty( $neuronsLogo ) ): ?>
				    <img src="<?php echo esc_url($neuronsLogo['url']); ?>" alt="<?php echo esc_attr($neuronsLogo['alt']); ?>" />
					<?php endif; ?>
				</div>
				<div id="primaryNav" class="">
					<?php if($menuitems): ?>
						<?php foreach ($menuitems as $menuitem):?>
							<?php if ($menuitem->menu_item_parent == "0"): ?>
								<?php $title = $menuitem->title;?>
								<?php $target = $menuitem->target;?>
								<?php $link = $menuitem->url;?>
								<?php if($menuitem->ID === 77):?>
									<a class="navdesktop
								<?php global $wp; echo ( strpos( trailingslashit( home_url( $wp->request ) ), $link ) !== false ) ? 'active' : ''; ?>" data-myid="<?php echo $menuitem->ID; ?>"><?php echo $title;?></a>
								<?php else :?>
								<a class="navdesktop
								<?php global $wp; echo ( strpos( trailingslashit( home_url( $wp->request ) ), $link ) !== false ) ? 'active' : ''; ?>" data-myid="<?php echo $menuitem->ID; ?>" href="<?php echo $link; ?>"><?php echo $title;?></a>
								<?php endif;?>
							<?php else: ?>
								<?php $title = $menuitem->title;?>
								<?php $target = $menuitem->target;?>
								<?php $link = $menuitem->url;?>
								<meta name="submenuitem" data-parent="<?php echo $menuitem->menu_item_parent;?>" data-title="<?php echo $title; ?>" data-target="<?php echo $target; ?>" data-link="<?php echo $link; ?>" data-myid="<?php echo $menuitem->ID; ?>">
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>	
					<a href="" rel="nofollow" target="_blank">
						<button class="button primary-btn nav-btn"><strong>Sign up</strong></button>
					</a>
				</div>
			</div>
		</div>
		<div class="hidden moving" id="submenu">
			<div id="submenuitems"></div>
		</div>
		<div id="burgerContainer" class="container">
			<div class="mobilelogo">
				<?php $neuronsLogo = get_field('logo', 'option');
				if( !empty( $neuronsLogo ) ): ?>
				<img src="<?php echo esc_url($neuronsLogo['url']); ?>" alt="<?php echo esc_attr($neuronsLogo['alt']); ?>" />
				<?php endif; ?>
			</div>
			<button id="burgerButton" class="" type="button" data-togglethisid="navContentMobile" toggler>
				<span class="burger-line top-line overlayMenuButton"></span>
				<span class="burger-line middle-line overlayMenuButton"></span>
				<span class="burger-line bottom-line overlayMenuButton"></span>
			</button>
		</div>
		<div id="navContentMobile" class="hidden moving container">
			<?php if($menuitems): ?>
				<div id="mobileNavigation" >
					<?php foreach ($menuitems as $menuitem):?>
						<?php if ($menuitem->menu_item_parent == "0"): ?>
							<?php $title = $menuitem->title;?>
							<?php $target = $menuitem->target;?>
							<?php $link = $menuitem->url;?>
							<?php if($menuitem->ID === 77 || $menuitem->ID === 66):?>
								<div>
									<button class="mobileItem dropdown" data-myid="<?php echo $menuitem->ID; ?>"><?php echo $title;?></button>
									<div id="<?php echo $menuitem->ID; ?>" class="subitems-container hidden" data-parent="<?php echo $menuitem->ID; ?>"></div>	
								</div>
								<?php else : ?>
									<div>
									<button class="mobileItem" data-myid="<?php echo $menuitem->ID; ?>"> <a href="<?php echo $link;?>"><?php echo $title;?></a></button>
								</div>
								<?php endif;?>
						<?php else: ?>
							<?php $title = $menuitem->title;?>
							<?php $target = $menuitem->target;?>
							<?php $link = $menuitem->url;?>
							<meta name="mobilesubitem" data-parent="<?php echo $menuitem->menu_item_parent;?>" data-title="<?php echo $title; ?>" data-target="<?php echo $target; ?>" data-link="<?php echo $link; ?>" data-myid="<?php echo $menuitem->ID; ?>">
						<?php endif; ?>
					<?php endforeach; ?>
					<button class="button primary-btn mobile-nav-btn"><strong>Sign up</strong></button>
				</div>
			<?php endif; ?>
		</div>
	</nav>
</body>