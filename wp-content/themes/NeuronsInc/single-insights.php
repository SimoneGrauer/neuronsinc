<?php 

get_header();
?>
<main id="main" class="site-main">
<div class="sub-hero">
    <div class="sub-hero-content container site-padding-both">
        <h1><?php the_field('headline');?></h1>
        <p><?php the_field('small_text');?></p>
    </div>
</div>
</main>
<?php
get_footer();