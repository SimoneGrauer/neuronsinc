<div id="banner" class="lightPurple site-padding-both">
    <div class="banner-container container">
        <div class="banner-text-content site-padding-bottom">
            <h2><?php echo $smallHeadline;?></h2>
            <h1><?php echo $headline;?></h1>
            <p><?php echo $text;?></p>
            <div class="banner-cta-buttons cta-buttons-container">
            <?php if($download) : ?>
                <a href="<?php echo esc_url( $download['url'] );?>" download>
                    <button class="button secondary-btn"><?php echo $downloadBtnText;?></button>
                </a>
            <?php endif; ?>
            <?php if($bannerButton) : ?>
                <a href="<?php echo esc_url( $bannerButton['url'] ); ?>" rel="nofollow" target="_blank">
                    <button class="button primary-btn"><?php echo $bannerBtnText; ?></button>
                </a>
            <?php endif; ?>
            </div>
        </div>
        <?php if( $bannerImage): ?>
        <div class="banner-image">
            <img src="<?php echo esc_url($bannerImage['url']); ?>" alt="<?php echo esc_attr($bannerImage['alt']); ?>" />
        </div>
        <?php endif; ?>
    </div>
</div>