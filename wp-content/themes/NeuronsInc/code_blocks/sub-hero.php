<div class="sub-hero container">
    <div class="sub-hero-content site-padding-both">
        <h2><?php the_field('small_headline');?></h2>
        <h1><?php the_field('headline');?></h1>
    </div>
</div>