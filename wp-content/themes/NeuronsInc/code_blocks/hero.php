<div class="hero-background-image">
        <div class="hero-container container site-padding-both">
            <div class="hero-text-content site-padding-bottom ipad">
                <h2><?php the_field('hero_small_headline');?></h2>
                <h1><?php the_field('hero_headline');?></h1>
                <p><?php the_field('text');?></p>
                <?php $downloadBtn = get_field( 'download_file' ); ?>
                <?php $downloadTxt = get_field('banner_download_text')?>
                <div class="cta-buttons-container">
                <?php if($downloadBtn):?>
                <a href="<?php echo esc_url( $downloadBtn['url'] ); ?>" rel="nofollow" target="_blank">
                    <button class="button secondary-btn"><?php echo $downloadTxt; ?></button>
                </a>
                <?php endif;?>
                <?php $button = get_field( 'button' ); ?>
                <?php $buttonTxt = get_field('button_text');?>
                <?php if($button):?>
                <a href="<?php echo esc_url( $button['url'] ); ?>" rel="nofollow" target="_blank">
                    <button class="button primary-btn"><?php echo $buttonTxt; ?></button>
                </a>
                <?php endif;?>
                </div>
            </div>
            <?php $image = get_field('image'); ?>
            <?php if( $image ): ?>
            <div class="hero-image">
                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
            </div>
            <?php endif; ?>
            <?php $mobileVideo = get_field( 'video_mobile' )?>
            <?php if( $mobileVideo ) : ?>
			<div class="hero-mobile-video">
				<video class="single-video" width="100%"  preload autoplay loop muted>
					<source src="<?php echo $mobileVideo?>" type="video/mp4"/>
				</video>
			</div>
            <?php endif;?>
            <?php $heroVideo = get_field( 'video' )?>
            <?php if( $heroVideo ) : ?>
			<div class="hero-video">
				<video class="single-video" width="100%"  preload autoplay loop muted>
					<source src="<?php echo $heroVideo?>" type="video/mp4"/>
				</video>
			</div>
            <?php endif;?>
        </div>
    </div> 