<footer>
    <div id="footer" class="">
        <div class="footer-grid container">
            <div class="footer-text-content">
                <div class="footer-office footer-padding">
                    <h3><?php the_field('Headline', 'options');?></h3>
                    <?php if ( have_rows( 'company_content', 'option' ) ) : ?>
	                        <?php while ( have_rows( 'company_content', 'option' ) ) : the_row(); ?>
                                <div class="footer-item-container">
                                    <?php 
                                    $footerIcon = get_sub_field('icon');
                                    if( !empty( $footerIcon ) ): ?>
                                        <img src="<?php echo esc_url($footerIcon['url']); ?>" alt="<?php echo esc_attr($footerIcon['alt']); ?>" />
                                    <?php endif; ?>
                                    <p><?php the_sub_field('text');?></p>
                                </div>   
	                        <?php endwhile; ?>
                        <?php else : ?>
	                        <?php // no rows found ?>
                        <?php endif; ?>
                </div>
                <div class="footer-solutions footer-padding">
                    <h3><?php the_field('solutions_headline', 'options');?></h3>
                    <?php if( have_rows('solutions_links', 'options') ):
                        while( have_rows('solutions_links', 'options') ) : the_row();?>
                        <div class="solutionLinks">
                        <?php
                            $solutionLink = get_sub_field( 'link' ); ?>
                            <a class="link" href="<?php echo esc_url( $solutionLink['url'] ); ?>" rel="nofollow" target="_blank">
                               <?php echo esc_html( $solutionLink['title'] ); ?>
                            </a>
                        </div>
                            <?php
                        endwhile;
                    else :
                    endif;?>
                </div>
                <div class="footer-policies footer-padding">
                    <h3><?php the_field('policies_headlines', 'options');?></h3>
                    <?php if ( have_rows( 'links_to_policies', 'option' ) ) : ?>
	                        <?php while ( have_rows( 'links_to_policies', 'option' ) ) : the_row(); ?>
                                <div class="">
                                    <?php 
                                    $link = get_sub_field('link');
                                    if( $link ): ?>
                                        <a class="link" href="<?php echo esc_url( $link['url'] ); ?>"><?php echo($link['title']) ;?></a>
                                    <?php endif; ?>
                                </div>
	                        <?php endwhile; ?>
                        <?php else : ?>
	                        <?php // no rows found ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="footer-subscribe footer-padding">
                <h2>JOIN OUR MAILINGLIST</h2>
                <p>Subscribe to our marketing newsletter to get the latest tips and advice delivered to your inbox each month! </p>
                <form action="">
                    <input type="text" name="" id="" placeholder="Your email">
                    <button class="button primary-btn">Subscribe</button>
                </form>
            </div>
        </div>
        <div class="copyright">
            <p>Copyright © Neurons Inc <?php echo date("Y");?>. All Rights Reserved</p>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/navigation.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/mobileNav.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/logoSlider.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/solutionsFrontpage.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/filterinsights.js"></script>


</body>
</html>