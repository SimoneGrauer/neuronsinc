<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'neuronsinc' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aB`InI}BE~C2OX.z,~c;]bn.x}3<8/Xn=K{BZ-i6WU! ]Zqdqrr{$w+R^{>RV;xD' );
define( 'SECURE_AUTH_KEY',  'USBR2TR>NW#C5Gb4aTP*N?Od6%A/DQ>,d=8I3jJ2u<{{N^5B4{-vYD.Ov+E+g` _' );
define( 'LOGGED_IN_KEY',    '.wCxVz,weo72Kvl^Y~IDIBs8D=D)WiP+}o]kG:fb_5^y7H)Qp2#Pn-sA,^QS!Jm.' );
define( 'NONCE_KEY',        ' lSnDGqDbv~d<]Jj/:6-+w]cB-F+BmSwnq_Tqp/H!-W,)hj!_c>a*nWPv^6Rum*Y' );
define( 'AUTH_SALT',        'U_BYmD=zV%[/ppg7EPAQ|R|[s^glsv}fA.EXzI5ggkq1Omt[F2Y`QEs%|4WcadM$' );
define( 'SECURE_AUTH_SALT', 'e4vm!sTxBsi9jMti|{&/AT)8)UmQasL?^eo6TFq)V%DJ^$8=XT54IQ#/sZAq? ,C' );
define( 'LOGGED_IN_SALT',   'jD@~Om)g8ouw(xVfO*YZy(9%#aam3bE(5HWd0s.H91P{s<LcVpOIK^BFLSip3X%P' );
define( 'NONCE_SALT',       '4@Tu2nE}DGkjv2Dfq/6QkNqX~:UT{L>e9?aG_Px_UoP( 9g?YVq!?L$[`C64wf#x' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
